from __future__ import absolute_import
import json
import ConfigParser
import os
from traverse.models.helpers import absolute_path as abs_path

class Settings(object):
	"""An object in app that represents all settings and strategies"""

	def __init__(self, file=None):
		super(Settings, self).__init__()
		if file:
			self.load_json(abs_path(file))

	def update(self, json):
		self.__dict__.update(json)
		return self
	
	def load_json(self, file):
		with open(file, "r") as f:
			self.__dict__.update(json.loads(f.read()))

	def load_config(self, file):
		config = ConfigParser.ConfigParser()
		config.read('file')
		self.__dict__.update(config._sections)

	def save_state(self):
		config = ConfigParser.ConfigParser()
		config._sections.update(self.__dict__)
		with open(os.path.join(os.environ["HOME"], ".binconfig"), "w") as f:
			config.write(f)

	@property
	def politics(self):
		politics = []
		politics.append(self.bin_settings["errors"]["on_delete"]["overflow"])
		politics.append(self.bin_settings["errors"]["on_restore"]["overflow"])
		politics.append(self.bin_settings["errors"]["on_restore"]["folder_missing"])
		politics.append(self.trash_settings["before_action"])
		politics.append(self.trash_settings["errors"]["access"])
		politics.append(self.trash_settings["errors"]["name_duplicates"])
		return set(politics)

	@politics.setter
	def politics(self, val):
		self.bin_settings["errors"]["on_delete"]["overflow"] = val
		self.bin_settings["errors"]["on_restore"]["overflow"] = val
		self.bin_settings["errors"]["on_restore"]["folder_missing"] = val
		self.trash_settings["before_action"] = val
		self.trash_settings["errors"]["access"] = val
		self.trash_settings["errors"]["name_duplicates"] = val

	@property
	def access(self):
		return self.trash_settings["errors"]["access"]


	@access.setter
	def access(self, val):
		self.trash_settings["errors"]["access"] = val

	@property
	def silencio(self):
		return self.trash_settings["errors"]["access"] == "skip" and self.bin_settings["errors"]["on_restore"]["folder_missimg"] == "create" and self.bin_settings["errors"]["on_restore"]["name_duplicates"] == "skip"

	@silencio.setter
	def silencio(self, flag):
		if flag:
			self.trash_settings["errors"]["access"] = "skip"
			self.bin_settings["errors"]["on_restore"]["folder_missimg"] = "create"
			self.bin_settings["errors"]["on_restore"]["name_duplicates"] = "skip"

