from .settings import Settings
from traverse.models.trashbin.bin import Bin
import logging
from traverse.models.public import Q
import os
import sys


class App(object):
	"""This is the class that implements all app functionality"""

	def __init__(self, name):
		super(App, self).__init__()
		
		self.name = name
		self.settings = Settings(os.path.join(os.path.dirname(__file__), "defaults.json"))
		self.root = os.path.join(os.path.expanduser("~"), "Bins", self.name)
		self.bin = Bin(self.root, self.settings)

		FORMAT = '%(asctime)-15s %(levelname)s: %(message)s at `%(funcName)s`'
		logging.basicConfig(format=FORMAT, level=logging.DEBUG)


	@property
	def logger(self):
		return logging.getLogger("app")
		
	
	def remove(self, path):
		self.logger.info("Removing file at %s", path)
		try:
			file = Q(path)
			self.logger.info("File %s was captured", file.path)
			self.bin.add(file)
			self.logger.info("Going to save bin's state")
			self.bin.save()
			self.logger.info("State saved")
		except Exception as e:
			self.logger.error("Exception of type %s occurred", e)
			print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
			raise e
		

	def restore(self, pattern="*"):
		trashpile = list(self.bin.grep(pattern))
		self.logger.info("Retrievieng trashed files")
		if not trashpile:
			self.logger.warning("Trashfile with that name wasn't found")
		for trash in trashpile:
			self.logger.info("Attempting to restore trashed file %s", trash.descriptor)
			self.bin.restore_files(trash.descriptor)
		self.bin.save()
				

	def inspect(self, pattern="*"):
		content = list(self.bin.grep(pattern))
		return content

	def dispose(self, pattern="*"):
		descriptors = list(self.bin.grep(pattern))
		self.logger.info("Retrievieng trashed files. Found %s", len(descriptors))
		if not descriptors:
			self.logger.error("File wasn't found")		
		for trash in descriptors:
			self.logger.info("Attempting to dipose trashed file %s", trash.descriptor)
			trash.dispose()
			del self.bin.content[trash.descriptor]
		self.bin.save()

	@property
	def dummify(self):
		return self.bin.recycler.dried and self.bin.dried

	@dummify.setter
	def dummify(self, val):
		self.bin.recycler.dried = val
		self.bin.dried = val

	@property
	def verbosity(self):
		return self.logger.getEffectiveLevel()

	@verbosity.setter
	def verbosity(self, flag):
		if flag:
			logging.getLogger('').setLevel(logging.INFO)
		else:
			logging.getLogger('').setLevel(logging.CRITICAL)



		
		