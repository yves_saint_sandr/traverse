import os
from .app import App
from .parser import Parser
from traverse.models.helpers import absolute_path as abs_path

APP_DIR = os.path.dirname(os.path.abspath(__file__))

def start():
	app = App(APP_DIR)
	parser = Parser()

	args = parser.parse_args()
	if args.config_file:
		app.settings.load_json(args.config_file)
	if args.dry_run:
		app.dummify = True
	if args.silent:
		app.verbosity = False
		app.settings.silencio = True
	if args.verbose:
		app.verbosity = True
	if args.interactive:
		app.settings.politics = "ask"
	if args.force:
		app.settings.access = "force"
	if args.command == "inspect":
		app.inspect(args.parameter or "*")
	elif args.command == "remove":
		app.remove(args.parameter)
	elif args.command == "restore":
		app.restore(args.parameter or "*")
	elif args.command == "dispose":
		app.dispose(args.parameter or "*")
	else:
		app.logger.error("Command {} is not recognizible".format(args.command))

	app.bin.save()

