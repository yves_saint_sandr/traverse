import argparse, readline

class Parser(object):
	"""docstring for Psrser"""
	def __init__(self):
		super(Parser, self).__init__()
		
		self.parser = argparse.ArgumentParser()
		self.group = self.parser.add_mutually_exclusive_group()
		self.parser.add_argument("command", 
				help="command to be executed",
				choices=["remove","restore","inspect", "dispose"],
				type=str)
		self.parser.add_argument("parameter", 
				help="relative or absolute path to the processed file or trash descriptor",
				type=str,
				nargs="?",
				default="")
		self.parser.add_argument("-f", "--force", 
				action="store_true",
				help="ignore nonexistent files and arguments, never prompt")
		self.parser.add_argument("-i", "--interactive", 
				action="store_true",
				help="prompt before every operation")
		self.group.add_argument("-v", "--verbose", 
				action="store_true",
				help="display detailed logging info")
		self.group.add_argument("-s", "--silent", 
				action="store_true",
				help="execute silently")
		self.group.add_argument("-d", "--dry-run", 
				action="store_true",
				help="display detailed info while not doing anything")
		self.parser.add_argument("-c", "--config",
				dest="config_file", 
				help="spicify the config file to use")

		readline.parse_and_bind("tab: complete")

	def parse_args(self):
		return self.parser.parse_args()