import os
import copy_reg
import types
from file import File
from ..helpers import absolute_path
import logging, stat
from multiprocessing import Pool, Manager
import functools

class Directory(File):
	"""class that represents UNIX directory"""

	def __init__(self, path, depth=100):
		super(Directory, self).__init__(path)

		self.type = "directory"
		depth -= 1
		self.path = path

		if depth:
			from ..public import Q
			self.content = {file: Q(os.path.join(self.path, file), depth) for file in os.listdir(self.path)}
		else:
			self.content = {}

	@property
	def logger(self):
		return logging.getLogger("app.file.directory")

	def __getitem__(self, key):
		if key in self.content:
			return self.content[key] 
		else:
			raise OSError("File can't be found")

	def __eq__(self, other):
		return self.path == other.path

	def _pickle_method(m):
		if m.im_self is None:
			return getattr, (m.im_class, m.im_func.func_name)
		else:
			return getattr, (m.im_self, m.im_func.func_name)

	copy_reg.pickle(types.MethodType, _pickle_method)

	def _refresh(self):
		self.content = {file: Q(os.path.join(self.path, file), depth) for file in os.listdir(self.path)}
		
	def _remove(self, files, force):
		try:
			self.content[files].remove(parallel=False)
			del self.content[files]
		except Exception as e:
			self.logger.error("Exception %s occured", e)
			if force:
				self.logger.warning("Deleting file %s forcefully", files)
				self.content[files].remove(force=True)
				del self.content[files]
				self.logger.info("Succesfully deleted")
			else:
				self.logger.error("Can't delete protected file %s", files)
		
	def remove(self, force=False, parallel=False):
		self.logger.info("Removing directory %s with force: %s", self.name, force)

		if parallel and self.content:
			pool = Pool(len(self.content))
			pool.map(functools.partial(self._remove, force=force), self.content.keys())
			pool.close()
			pool.join()
		else:
			for files in self.content.keys():
				try:
					self.content[files].remove(parallel=False)
					del self.content[files]
				except Exception as e:
					self.logger.error("Exception %s occured", e)
					if force:
						self.logger.warning("Deleting file %s forcefully", files)
						self.content[files].remove(force=True)
						del self.content[files]
						self.logger.info("Succesfully deleted")
					else:
						self.logger.error("Can't delete protected file %s", files)

		self._refresh()
		self.logger.debug("Length: %s\nContents: %s", len(self.content), self.content)
		if len(self.content):
			self.logger.error("Can't delete not-empty directory %s", self.name)
			raise OSError("Directory {} is not empty".format(self.name))
		else:
			try:
				self.logger.warning("Deleting directory %s", self.name)
				os.rmdir(self.path)
				self.logger.info("Succesfully deleted %s", self.name)
			except:
				if force:
					self.logger.warning("Deleting directory %s forcefully", self.name)
					self.logger.warning("Changing %s access permissions", self.name)
					os.chmod(self.path, stat.S_IWRITE )
					self.logger.warning("Deleting directory %s", self.name)	
					os.rmdir(self.path)
					self.logger.info("Succesfully deleted")
				else:
					self.logger.error("Can't delete protected directory %s", self.name)
					raise OSError("Can't delete protected directory")	

	def write(self):
		raise AttributeError( "'Directory' object has no attribute 'write'" )

	def read(self):
		raise AttributeError( "'Directory' object has no attribute 'read'" )

	def append(self, file):
		self.content.update({file.name: file})

	def truncate(self, string):
		raise AttributeError( "'Directory' object has no attribute 'truncate'" )

	def as_json(self):
		return {
			"name": self.name,
			"type": self.type,
			"path": self.path,
			"content": [
					{ 
					"name": name,
					"type": val.type,
					"path": val.path
					} 
				for name, val in self.content.items()
			],
			
		}