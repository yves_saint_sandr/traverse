import os
from file import File
from ..helpers import absolute_path
import logging, stat

class Link(File):
	"""class that represents the Soft Link on python"""

	def __init__(self, path):
		super(Link, self).__init__(path)

		self.type = "link"
		self.destination = os.path.realpath(self.path)

		#self.logger = logging.getLogger("app.file.directory")

	def remove(self, force=False, parallel=True):
		self.logger.info("Deleting link with force: %s", force)
		try:
			os.remove(self.path)
		except Exception as e:
			if force:
				self.logger.warning("Changing %s access permissions", self.name)
				try:
					os.chmod(self.path, stat.S_IWRITE )
				except Exception as e:
					self.logger.error("%s permissions can't be changed. Consider running this command as superuser", self.name)
					raise e
				os.remove(self.path)
				self.logger.info("Succesfully removed")
			else:
				raise e
		self.logger.info("Successfully deleted file %s", self.name)

	def write(self):
		raise AttributeError( "'Link' object has no attribute 'write'" )

	def read(self):
		raise AttributeError( "'Link' object has no attribute 'read'" )

	def append(self, string):
		raise AttributeError( "'Link' object has no attribute 'append'" )

	def truncate(self, string):
		raise AttributeError( "'Link' object has no attribute 'truncate'" )


		