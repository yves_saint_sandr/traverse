import os
from ..helpers import absolute_path
import logging, stat

class File(object):
	"""A Class that represents all types of files in a UNIX system"""

	def __init__(self, path):
		super(File, self).__init__()

		self.type = "file"
		self.path = absolute_path(path)
		self.name = os.path.basename(self.path) 

		statinfo = os.stat(self.path)
		self.size = statinfo.st_size
		self.owner = statinfo.st_uid


	def __hash__(self):
		return hash(self.path)

	def __eq__(self, other):
		return self.path == other.path

	def remove(self, force=False, parallel=False):
		logging.getLogger("app.file").info("Deleting file with force: %s", force)
		try:
			os.remove(self.path)
		except Exception as e:
			logging.getLogger("app.file").error("Exception %s occurred", e)
			if force:
				logging.getLogger("app.file").warning("Changing %s access permissions", self.name)
				try:
					os.chmod(self.path, stat.S_IWRITE )
				except Exception as e:
					logging.getLogger("app.file").error("%s permissions can't be changed. Consider running this command as superuser", self.name)
					raise e
				
				os.remove(self.path)
				logging.getLogger("app.file").info("Succesfully removed")
			else:
				raise e
		logging.getLogger("app.file").info("Successfully deleted file %s", self.name)
		

	def read(self, size):
		with open(self.path, "r") as f:
			return f.read(size)

	def write(self, string):
		"""Writes a string to a file and overriding the old data"""
		with open(self.path, "w+") as f:
			f.write(string)

	def append(self, string):
		"""Append the string to the file"""
		logging.getLogger("app.file").info("Attempting to append data to file %s", self.name)
		logging.getLogger("app.file").debug("Appending file %s placed at %s", self.name, self.path)
		with open(self.path, "w") as f:
			f.write(string)

	def truncate(self, string):
		self.write(string)

	def as_json(self):
		return {
			"name": self.name,
			"type": self.type,
			"path": self.path,
		}

	