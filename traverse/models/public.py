import os
from files.file import File
from files.directory import Directory
from files.link import Link
from helpers import absolute_path

def Q(path, depth=100, force=False):
	abs_path = absolute_path(path)

	if not os.path.exists(abs_path):
		if force:
			open(abs_path, "w")
		else:
			raise OSError("File can't be found")

	if os.path.islink(abs_path):
		return Link(abs_path)
	elif os.path.isfile(abs_path):
		return File(abs_path)
	elif os.path.isdir(abs_path):
		return Directory(abs_path, depth)
	else:
		raise OSError("File type is not recognizible")
