import tarfile
import os
from trash import Trash
from ..helpers import confirm, drier

class Recycler(object):
	"""The followind class in used for special processing before deleting file"""

	def __init__(self, bin_):
		super(Recycler, self).__init__()

		self.bin = bin_
		self.dried = False

	@drier
	def recycle(self, file):
		"""This method is called before the file will be deleted
			this is where you can add ypur custom logic like compressing or encryption
			this method accepts File object and return the Trash object"""
		
		desc = self.get_descriptor(file)
		path = os.path.join(self.bin.path, desc)
		politic = self.bin.settings.trash_settings["size"]
		self.bin.logger.debug("Creating archived file with name %s", desc)
		if politic == "compress":
			with tarfile.open(path + ".tar", "w:gz") as tar:
				tar.add(file.path, arcname=os.path.basename(file.path))
		else:
			with tarfile.open(path + ".tar", "w") as tar:
				tar.add(file.path, arcname=os.path.basename(file.path))
		trash = Trash(file, path, desc)
		self.bin.logger.debug("Arcive %s succesfully crated", desc)
		try:
			file.remove()
		except Exception as e:
			self.bin.logger.error("Exception %s occured", e)
			if self.bin.settings.access == "ask":
				if confirm(prompt="The following file is protected. Do you wish to delete it anyway? [Y/n] "):
					self.bin.logger.warning("Attempting to remove forcefully")
					file.remove(force=True)
					self.bin.logger.info("Succesfully removed")
					return trash
				else:
					self.bin.logger.error("Can't delete protected file")
			elif self.bin.settings.access == "force":
				self.bin.logger.warning("Attempting to remove forcefully")
				file.remove(force=True)
				self.bin.logger.info("Succesfully removed")
				return trash
			elif self.bin.settings.access == "error":
				raise e
		else:
			return trash
		

	def get_descriptor(self, file):
		politic = self.bin.settings.trash_settings["errors"]["name_duplicates"]
		self.bin.logger.debug("Retirevieng politics. Current politic is %s", politic)
		if self.bin.settings.trash_settings["errors"]["name_duplicates"] == "ask":
			self.bin.logger.info("Asking for trash name")
			name = raw_input("Enter descriptor: ")
			if name in self.bin.content.keys():
				raise KeyError("The trashfile with this name already exists")
			return name
		elif politic == "inc":
			self.bin.logger.debug("Counting occerunces")
			if file.name in self.bin.content.keys():
				return file.name + "_" + str(self.bin.raw_contents(self.bin.content).count(file.name))
			else:
				return file.name
		return file.name


	def get_destination(self, trash):
		if not os.path.exists(trash.file_placement):
			politic = self.bin.settings.bin_settings["errors"]["on_restore"]["folder_missing"]
			if politic == "create":
				os.makedirs(trash.file_placement)
				return trash.file_placement
			elif politic == "ask":
				if confirm(prompt="The restoring directory is missing. Would you like to create one? [Y/n]"):
					os.makedirs(trash.file_placement)
					return trash.file_placement
				else:
					raise OSError("Directory doesn't exist")
			else:
				raise OSError("Directory doesn't exist")
		if os.path.exists(trash.file_path):
			politic = self.bin.settings.bin_settings["errors"]["on_restore"]["name_duplicates"]
			if politic == "ask":
				self.bin.logger.info("Asking for trash name")
				name = raw_input("Enter new name: ")
				if name in os.listdir(trash.file_placement):
					raise KeyError("File with that name already exists")
				return name
			else:
				raise OSError("Such name already exists")
		return trash.file_placement

	@drier
	def restore(self, trash):
		dest = self.get_destination(trash)
		if self.bin.is_enough_space_for(trash.file_placement, trash.previous_size):	
			with tarfile.open(trash.path + ".tar") as tar:
				tar.extractall(trash.file_placement)
		else:
			raise OSError("Not enough space")
