import os
import pickle
from ..helpers import regexify, drier, confirm
from ..public import Q
from recycler import Recycler
from fnmatch import translate
import logging
import re
import sys

class Bin(object):
	"""A collection for Trash objects"""


	def __init__(self, root, settings):
		super(Bin, self).__init__()

		self.path = root
		
		if not os.path.exists(self.path):		
			os.makedirs(self.path)

		if not os.path.exists(os.path.join(self.path, ".bin")):
			os.makedirs(os.path.join(self.path, ".bin"))

		self.settings = settings

		self.dumpfiles = os.path.join(self.path, ".bin")

		self.recycler = Recycler(self)
		self.dried = False

		self.content = self.get_contents()

	@property
	def logger(self):
		return logging.getLogger("app.bin")

	@property
	def size_in_bytes(self):
		return os.path.getsize(self.path)

	@property
	def is_free_enough(self):
		return self.size_in_bytes > int(self.settings.bin_settings["max_size"])

	def is_enough_space_for(self, folder, memory):
		space = os.statvfs(folder).f_bavail  
		return memory < space

	def __str__(self):
		text = ""
		for trash in self.content.values():
			text+=str(trash)
		return text

	def __len__(self):
		return len(self.content)

	def add(self, file):
		if self.is_enough_space_for(self.path, file.size) and self.is_free_enough:
			if self.settings.trash_settings["before_action"] == "ask":
				if confirm(prompt="Are you sure you want to delete {}? [Y/n] ".format(file.name)):
					trash = self.recycler.recycle(file)
					self.content[trash.descriptor] = trash
				return self
			else:
				try:
					trash = self.recycler.recycle(file)
				except Exception as e:
					if self.settings.trash_settings["errors"]["access"] == "skip":
						return self
					else:
						print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
						raise e
				self.content[trash.descriptor] = trash
				return self
		else:
			raise OSError("Not enough space")

	def __getitem__(self, descriptor):
		return self.content[descriptor]

	def __contains__(self, item):
		return item in self.content

	def grep(self, pattern="*"):
		self.logger.debug("Compiled regex %s", regexify(pattern))
		for desc in self.content:
			if re.match(re.compile(regexify(pattern), re.I | re.S), desc):
				yield self.content[desc]

	def restore_files(self, *descriptors):
		for descriptor in descriptors:
			self.logger.info("Restoring file %s", descriptor)
			self.recycler.restore(self.content[descriptor])
			self.logger.debug("File %s was succesfully restored", descriptor)
			self.content[descriptor].dispose()
			del self.content[descriptor]
			self.logger.debug("Trash object was successfully removed")

	def get_contents(self):
		trashpile = os.listdir(self.dumpfiles)
		content = {}
		if trashpile:
			for trash in trashpile:
				item = pickle.load(open(os.path.join(self.dumpfiles, trash), "rb"))
				content.update({ item.descriptor: item })
		return content

	def raw_contents(self, content):
		return [trash.split("_")[0] for trash in content]

	def save(self):
		self.logger.info("Removing temporary files for saving")
		for file in os.listdir(self.dumpfiles):
			os.remove(os.path.join(self.dumpfiles, file))
		self.logger.info("Going to save content %s", self.content)
		for key in self.content:
			pickle.dump(self[key], open(os.path.join(self.dumpfiles, key), "wb"))
		self.logger.info("Saved succesfully")
		self.settings.save_state()

	def restore(self):
		self.content = self.get_contents()

	def cleanup_to(self, num):
		cleanup_for(int(self.settings.bin_settings["max_size"])-num)

	def cleanup_for(self, num):
		while not self.is_enough_space_for(self.path, num):
			order = self.settings.bin_settings["delete_order"]["order"]
			parameter = self.settings.bin_settings["delete_order"]["by"]
			if parameter == "deletion_date":
				if order == "asc":
					item = max(self.content, key=lambda x: x.deletion_date)
				else:
					item = min(self.content, key=lambda x: x.deletion_date)
			self.content[item.descriptor].dispose()
			del self.content[item.descriptor]

