import os
import datetime
import logging

class Trash(object):
	"""docstring for Trash"""
	def __init__(self, old_file, new_path, descriptor):
		super(Trash, self).__init__()

		self.descriptor = descriptor
		self.path = new_path
		
		self.file_path = old_file.path
		self.file_placement = os.path.dirname(old_file.path)
		self.previous_size = old_file.size
		self.previous_name = old_file.name

		self.current_size = os.stat(self.path + ".tar").st_size
		self.deletion_date = datetime.datetime.now()

	def __str__(self):
		return "\n-> {}\nDeleted from: {}\nAt: {}\n".format(self.descriptor.split("_")[:-1], self.file_placement, self.deletion_date)

	def dispose(self):
		os.remove(self.path + ".tar")

	def as_json(self):
		return {
			"name": self.previous_name,
			"placement": self.file_placement,
			"deleted_at": self.deletion_date,
			"path": self.file_path,
			"_now_at": self.path,
			"descriptor": self.descriptor,
		}

