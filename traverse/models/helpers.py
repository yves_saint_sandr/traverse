"""The following methods should be independent and available to use from outside"""
import os 
from fnmatch import translate

def absolute_path(path):
	"""Return an absolute path from string of any type"""
	realpath = os.path.expanduser(os.path.expandvars(path))

	if os.path.isabs(realpath):
		return realpath
	else: 
		return os.path.abspath(realpath)

def confirm(prompt="Are you sure?", yes=["Y", "YES"], no=["N", "NO"], default=False, ignorecase=True):
    
    answer = raw_input(prompt)
    if ignorecase:
        yes = [st.upper() for st in yes]
        no = [st.upper() for st in no]
        answer = answer.upper()

    if answer in yes:
        return True
    elif answer in no:
        return False
    else:
        return default

def regexify(string):
    if string.startswith("\\") and string.endswith("\\"):
        return "^" + string[1:-1] + "$"
    else:
        return "^" + translate(string) + "$"

def drier(func):
    def wrapper(self, *args, **kwargs):
        if self.dried:
            print("Executing {} at {}".format(func.__name__, args))
        else:
            return func(self, *args,**kwargs)
    return wrapper

