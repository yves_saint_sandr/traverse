import unittest
from units.test_files import TestFileMethods
from units.test_directories import TestTreeMethods
from units.test_bin import TestBinMethods
from units.test_settings import TestSettingsMethods

if __name__=="__main__":
    tests = [
        TestFileMethods,
        TestTreeMethods,
        TestBinMethods,
        TestSettingsMethods,
    ]
    suite = unittest.TestSuite(map(prepare_suite, tests))
    unittest.TextTestRunner(verbosity=0).run(suite)