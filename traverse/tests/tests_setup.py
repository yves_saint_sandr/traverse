import unittest
import os
import shutil
from traverse.models.public import Q
from traverse.models.trashbin.bin import Bin
from traverse.app.app import App
from traverse.app.start import APP_DIR

class TravesterTest(unittest.TestCase):

    def setUp(self):
        self.dirpath = os.getcwd()
        self.app = App(APP_DIR)
        os.makedirs("tree/branch/leaf/")
        os.makedirs("tree/branch/apple/")
        os.symlink(os.path.abspath("tree"), os.path.abspath("tree/branch/jump/"))
        os.makedirs("tree/hollow/")
        open("tree/hollow/squirrel.animal", 'w+').close()
        with open("tree/branch/apple/worm.animal", 'w+') as f:
            f.write("HAHAHA I'M GONNA SPOIL YOUR APPLE")
        self.tree = Q("tree")
        self.file = Q("tree/branch/apple/worm.animal")
        self.bin = self.app.bin

    def tearDown(self):
        shutil.rmtree('tree')

    @classmethod
    def prepare_suite(cls):
        return unittest.TestLoader().loadTestsFromTestCase(cls)

        