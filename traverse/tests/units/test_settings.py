import unittest
import os
from ..tests_setup import TravesterTest

class TestSettingsMethods(TravesterTest):

    def test_create_settings(self):
        self.settings = self.app.settings

        self.assertEquals(self.settings.app_settings["app"], "traverse")
        self.assertEquals(self.settings.bin_settings["errors"]["on_delete"]["overflow"], "error")

    def test_update_settings(self):
        self.app.settings.load_json(os.path.join(os.environ["HOME"],"sett.json"))
        self.assertEquals(self.app.settings.app_settings["version"], "1.0")