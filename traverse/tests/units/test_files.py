import unittest
import os
from traverse.models.files.file import File
from traverse.models.files.directory import Directory
from traverse.models.files.link import Link
from traverse.models.public import Q
from ..tests_setup import TravesterTest

class TestFileMethods(TravesterTest):

    def test_create_path(self):
        fullpath = "/home/alexander/Projects/traverse/traverse/"
        self.assertEqual(File("tree/hollow/squirrel.animal").path, os.path.join(fullpath, "tree/hollow/squirrel.animal"))
        self.assertEqual(File("~/Projects/traverse/traverse/tree/branch").path, os.path.join(fullpath, "tree/branch"))
        self.assertEqual(File("/home/alexander/Projects/traverse/traverse/tree").path, os.path.join(fullpath, "tree"))
        with self.assertRaises(OSError):
            File("abracadabra.tmp")

    def test_create_info(self):
        self.assertEqual(self.file.size, 33)
        self.assertEqual(self.file.owner, os.geteuid())
        self.assertEqual(self.file.name, "worm.animal")

    def test_select_type(self):
        self.assertEqual(type(Q("tree/hollow/squirrel.animal")), File)
        self.assertEqual(type(Q("~/Projects/traverse/traverse/tree/")), Directory)
        self.assertEqual(type(Q("tree/branch/jump")), Link)
