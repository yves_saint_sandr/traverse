import unittest
from traverse.models.files.directory import Directory
from traverse.models.files.link import Link
from traverse.models.public import Q
from ..tests_setup import TravesterTest

class TestTreeMethods(TravesterTest):

    def test_create_directory_tree(self):
        self.assertIsInstance(self.tree["branch"], type(Q("tree/branch")))
        self.assertIsInstance(self.tree["branch"]["leaf"], type(Q("tree/branch/leaf")))
        self.assertIsInstance(self.tree["hollow"]["squirrel.animal"], type(Q("tree/hollow/squirrel.animal")))
        with self.assertRaises(OSError):
            self.tree["branch"]["branch"]

    def test_recognize_links(self):
    	self.assertIsInstance(self.tree["branch"]["jump"], Link)

    def test_should_singleton(self):
    	self.assertEquals(self.tree, Q("tree"))
    	self.assertEquals(self.tree["branch"], Q("tree/branch"))
    	self.assertEquals(self.tree["branch"]["jump"], Q("tree/branch/jump"))

    
