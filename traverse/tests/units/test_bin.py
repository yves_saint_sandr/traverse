import unittest
import os
from ..tests_setup import TravesterTest
from traverse.models.trashbin.bin import Bin
from traverse.models.trashbin.trash import Trash
from traverse.models.public import Q

class TestBinMethods(TravesterTest):

	def test_bin_create(self):
		trash = self.file
		self.assertIsInstance(Bin("tmp", self.app.settings), Bin)

	def test_bin_delete(self):
		pass
		
	def test_bin_serialize(self):
		self.bin.save()
		self.bin.content.clear()
		self.bin.restore()
